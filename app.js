let express = require('express');
let path = require('path');
let logger = require('morgan');
let bodyParser = require('body-parser');

let index = require('./routes/index');
let users = require('./routes/users');
let files = require('./routes/files');
let playlists = require('./routes/playlists');

let publicFldr = path.join(__dirname, 'public');

let app = express();


//serve static files
app.use(express.static(publicFldr));

app.use((req, res, next) => {
  //do some logging here
  next();
});
//enable cors
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', index);
app.use('/users', users);
app.use('/files', files);
app.use('/playlists', playlists);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  let err = new Error('That Endpoint Was Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render('error');
});

module.exports = app;

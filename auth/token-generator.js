// sign with default (HMAC SHA256)
var jwt = require('jsonwebtoken');
let defissuer = "/kevcoder/media-server/";
let get = (issuer, subject, payload) =>
    jwt.sign(payload,
        '6ae655cd-e226-4f54-b438-b96706004ded',
        {
            expiresIn: '1h',
            subject: subject,
            issuer: issuer || defissuer
        }
    );

// sign with RSA SHA256
// var cert = fs.readFileSync('private.key');  // get private key
// var token = jwt.sign({ foo: 'bar' }, cert, { algorithm: 'RS256'});

// sign asynchronously
// jwt.sign({ foo: 'bar' }, cert, { algorithm: 'RS256' }, function(err, token) {
//   console.log(token);
// });

module.exports = {
    get: (issuer, subject, payload) => { return get(issuer, subject, payload); }
};
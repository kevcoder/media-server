var express = require('express');
var fs = require('fs');
var path = require('path');
var tree = require('directory-tree');
var findit = require('findit');
var router = express.Router();
var _ = require('lodash');
var os = require('os');
var slash = require('slash');

let assetsFldr = path.join(__dirname, '../public/assets');


let funcCorrectPath = (pathString) => {
    //turn these into relative paths
    let x = path.relative(assetsFldr, pathString);
    //if on windows change to the correct seperator
    if (os.platform() === 'win32') {
        x = slash(x)
    }
    return x
};

let funcModifyPaths = (children) => {
    if (children && _.isArray(children) && children.length > 0)
        _.each(children, (val, idx) => {
            if (val.path && _.isString(val.path)) {
                val.path = funcCorrectPath(val.path);
            }
            if (val.children)
                funcModifyPaths(val.children);
        });
};



/* GET files listing. */
router.get('/', function (req, res, next) {

    let foundTree = tree(assetsFldr);
    if (foundTree) {
        if (foundTree.path && _.isString(foundTree.path)) {
            //turn these into relative paths
            foundTree.path = funcCorrectPath(foundTree.path);
        };

        funcModifyPaths(foundTree.children);

    }
    res.send({ tree: foundTree });
});

router.get('/:rootPath', (req, res, next) => {
    let relativeUrl = decodeURI(req.params.rootPath);
    console.info(relativeUrl);
    console.info(assetsFldr);

    let fullPath = path.join(assetsFldr, relativeUrl);
    console.info(fullPath);

    let foundTree = tree(fullPath);

    if (foundTree) {
        if (foundTree.path && _.isString(foundTree.path)) {
            //turn these into relative paths
            foundTree.path = funcCorrectPath(foundTree.path);
        };

        funcModifyPaths(foundTree.children);
    }

    res.send({ tree: foundTree });
});

router.get('/retrieve/:filePath', function (req, res, next) {
    let relativeUrl = decodeURI(req.params.filePath);
    let fullPath = path.join(assetsFldr, relativeUrl);
    console.info(fullPath);

    var stream = fs.createReadStream(fullPath);
    stream.pipe(res);
});

module.exports = router;

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.send({
    navigation:
    [
      { path: './users', description: 'users' },
      { path: './files', description: 'files' },
      { path: './playlists', description: 'playlists' }
    ]
  });
});

module.exports = router;

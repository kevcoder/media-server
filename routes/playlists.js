let express = require('express');
let router = express.Router();
let jwt = require('../auth/token-generator');
let config = require('../config.js').config();
let bodyParser = require('body-parser');


let collectionName = 'playlist';

//get a connection to mongo
let dbPromise = require('mongodb').MongoClient.connect(config.db.connectionString);
let ObjectID = require('mongodb').ObjectID;

var jsonParser = bodyParser.json();

router.get('/', (req, res, next) => {
    res.status(403).send("I'm not that easy. Try /playlists/:playlistid instead");
});

router.get('/:playlistid', (req, res, next) => {

    let playlist_id = decodeURI(req.params.playlistid);
    dbPromise.then((db) => {
        db.collection(collectionName)
            .findOne({ _id: new ObjectID(playlist_id) })
            .then((val) => {
                if (val) {
                    res.send(val);
                }
                else {
                    res.sendStatus(404);
                }
            })
    });
});

router.post('/', jsonParser, (req, res, next) => {
    let playlist = req.body;
    let searchId = new ObjectID(playlist._id);
    playlist._id = searchId;
    dbPromise.then((db) => {
        db.collection(collectionName)
            .updateOne({ _id: searchId }, playlist, { upsert: true })
            .then((insertOpts) => {
                res.send({ playlistId: insertOpts.upsertedId || searchId });
            })
            .catch((error) => res.status(500).send(error))
    });
});

router.delete('/:playlistid', (req, res, next) => {
    let playlist_id = decodeURI(req.params.playlistid);
    dbPromise.then((db) => {
        db.collection(collectionName)
            .findOneAndDelete({ _id: new ObjectID(playlist_id) })
            .then((val) => {
                if (val) {
                    res.send(val);
                }
                else {
                    res.sendStatus(404);
                }
            })
    });
});

module.exports = router;
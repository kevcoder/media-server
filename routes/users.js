let express = require('express');
let router = express.Router();
let jwt = require('../auth/token-generator');
let config = require('../config.js').config();
let bodyParser = require('body-parser');
let ObjectID = require('mongodb').ObjectID;
let _ = require('lodash');

let dbPromise = require('mongodb').MongoClient.connect(config.db.connectionString);

var jsonParser = bodyParser.json();

router.use((req, res, next) => {
  console.info('user endpoint', req.url);
  console.info('using connectionString: ' + config.db.connectionString);
  next();
})

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.status(403).send("I'm not that easy. Try /users/:username instead.");
});

router.get('/:userId', (req, res, next) => {
  let decodedUserID = decodeURI(req.params.userId);
  let userObjectId = new ObjectID(decodedUserID);
  dbPromise.then((db) => {
    db.collection('users')
      .findOne({ _id: userObjectId })
      .then((val) => {
        if (val) {

          // let issuer = `${req.protocol}://${req.hostname}`;
          // let encodedJwt = jwt.get(issuer, val.userName, val);
          // let token = {
          //   token: encodedJwt
          //   , type: "bearer"
          //   , issuer: issuer
          // };
          res.send(val);

        }
        else {
          console.info(`GET /users/${decodedUserID} returned ${val}`);
          res.sendStatus(404);
        }
      })

  });

})

router.post('/', jsonParser, (req, res, next) => {
  let user = req.body;
  let id =  new ObjectID(user._id);
  delete user._id;
  console.info(user);
  dbPromise.then((db) => {
    db.collection('users')
      .updateOne({ _id: id }, user, { upsert: true })
      .then((updateWriteOpts) => {
        res.send(updateWriteOpts.upsertedId || id.toHexString());
      })
      .catch((error) => res.status(500).send(error))
  });
})

module.exports = router;

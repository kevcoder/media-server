var env = require('./environment.json');
var _ = require('lodash');
exports.config = function () {
    if (!env)
        throw "env is undefined: failed to load ./environment.json";

    if (!process || !process.env || !process.env.NODE_ENV) {
        console.info("process.env.NODE_ENV is undefined: defaulting to 'development'");
        process.env.NODE_ENV = "development";
    }

    let envConfig;
    _.forOwn(env, (value, key) => {
        if (key.trim().toLowerCase() === process.env.NODE_ENV.trim().toLowerCase())
            envConfig = value;
    });

    if (process.env.NODE_ENV === 'development') {
        console.info("env", typeof env, env);
        console.info("process.env.NODE_ENV", typeof process.env.NODE_ENV, process.env.NODE_ENV);
    }
    

    if (!envConfig)
        throw "envConfig is undefined";
    return envConfig;
}